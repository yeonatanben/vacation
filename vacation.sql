-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2021 at 11:48 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vacation`
--

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `userID` int(11) NOT NULL,
  `vacationID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`userID`, `vacationID`) VALUES
(2, 341),
(2, 342),
(2, 347),
(2, 358),
(19, 341),
(19, 342),
(19, 347),
(20, 342),
(20, 346),
(20, 347),
(20, 359),
(25, 341),
(25, 346),
(25, 358),
(25, 359);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(8) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `firstName`, `lastName`, `username`, `password`, `role`) VALUES
(1, 'David', 'Cohen', 'Davidc', '1234', 'admin'),
(2, 'Yoni', 'Levi', 'Yonil', '4321', ''),
(19, 'roni', 'chlifa', 'Ronic', '0000', ''),
(20, 'rivka', 'ben hur', 'Rivkab', '2000', ''),
(25, 'yacov', 'ben david', 'YBD', '2345', ''),
(34, 'yoni', 'ben', 'yonib', '1234', '');

-- --------------------------------------------------------

--
-- Table structure for table `vacation`
--

CREATE TABLE `vacation` (
  `vacationID` int(11) NOT NULL,
  `description` varchar(70) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `price` int(11) NOT NULL,
  `followersCount` int(3) NOT NULL,
  `picture` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vacation`
--

INSERT INTO `vacation` (`vacationID`, `description`, `destination`, `startDate`, `endDate`, `price`, `followersCount`, `picture`) VALUES
(341, 'A Journey in the Holy Land', 'Jerusalem', '2021-10-31', '2021-11-19', 800, 0, 'snowscat-qFVkazdQdZU-unsplash.jpg'),
(342, 'A trip to the City of Light', 'Paris', '2021-11-25', '2021-11-28', 599, 0, 'denys-nevozhai-UzagqG756OU-unsplash.jpg'),
(345, 'Real rest in Mexico', 'Mexico', '2021-12-19', '2021-12-24', 333, 0, 'yang-wewe-UBe-M4bBxjw-unsplash.jpg'),
(346, 'A trip to the City of Light', 'Alps', '2021-11-25', '2021-11-28', 900, 0, 'thomas-oldenburger-DAy_v2tXi6Q-unsplash.jpg'),
(347, 'Fascinating walk through the mysteries of the desert', 'Sahara desert', '2021-12-22', '2021-12-28', 345, 0, 'lucas-ludwig-vMxMkOgbUwU-unsplash.jpg'),
(358, 'Adventure in Venice', 'Venice', '2021-12-15', '2021-12-22', 876, 0, 'frankfurt-3581739_1280.jpg'),
(359, 'Magical vacation in Madrid', ' Madrid', '2021-12-22', '2021-12-25', 888, 0, 'yannes-kiefer-Gjdv7r62OLA-unsplash.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD KEY `userID` (`userID`,`vacationID`),
  ADD KEY `vacationID` (`vacationID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `vacation`
--
ALTER TABLE `vacation`
  ADD PRIMARY KEY (`vacationID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `vacation`
--
ALTER TABLE `vacation`
  MODIFY `vacationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_ibfk_1` FOREIGN KEY (`vacationID`) REFERENCES `vacation` (`vacationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `followers_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
