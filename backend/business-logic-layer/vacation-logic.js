
const dal =require("../data-access-layer/dal");
const { builtinModules } = require("module");
const io=require("socket.io");

function getAllVacationAsync(){
    const Vacation= dal.executeQueryAsync("select * from vacation  order by followersCount desc");
    return Vacation;
}





function getVacationByIdAsync(id){ 
    const allVacation= dal.executeQueryAsync(`SELECT vacation.* ,followers.userID 
    from vacation LEFT JOIN followers ON
    vacation.vacationID = followers.vacationID 
    and followers.userID=${id}
    order by followers.userID desc`);
    return allVacation;

}


function unFolloAsync(uid,vid){ 
    const unFollow= dal.executeQueryAsync(`
    DELETE FROM followers WHERE userID=${uid} and vacationID=${vid} 
    `);
    return unFollow;
}

function followAsync(uid,vid){
    const follow= dal.executeQueryAsync(`INSERT INTO followers(userID, vacationID) VALUES (${uid},${vid})`);
    return follow;

}

    


module.exports={
    getAllVacationAsync,
    getVacationByIdAsync,
    unFolloAsync,
    followAsync   
}