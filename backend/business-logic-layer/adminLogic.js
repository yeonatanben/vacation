const dal = require("../data-access-layer/dal");
const { builtinModules } = require("module");
const io = require("socket.io");
const { request } = require("express");
const { notifyClients } = require("./socket-logic");









function getAllVacationAdminAsync() {
    const allVacation =
        dal.executeQueryAsync("SELECT v.vacationID, v.description,v.destination,v.startDate,v.endDate,v.price,v.picture, count(f.userID) as countFollowers FROM vacation v left join followers f on v.vacationID = f.vacationID group by v.vacationID")
        // ("select * from vacation");
    return allVacation;
}

function getVacationByIdAsync(id) {
    const userVacation =
        dal.executeQueryAsync(`select * from vacation where vacationID=${id}`);
    return userVacation;
}



function postVacation(vacation) {

    const cmd = `insert into vacation values
    (null,'${vacation.description}','${vacation.destination}',
    '${vacation.startDate}','${vacation.endDate}',
    '${vacation.price}', 0,'${vacation.picture}')`;

    console.log(cmd);
    notifyClients();
    const newVacation= dal.executeQueryAsync(cmd);
    return newVacation;


}
function deleteVacation(id) {
    notifyClients();
    const deleted =
        dal.executeQueryAsync(`DELETE FROM vacation WHERE vacationID=${id}`);
    return deleted;
};

function editVacation(vacation) {

    let sql;
    if (vacation.picture) {
        sql = `UPDATE vacation SET
        vacationID='${vacation.vacationID}',
        description='${vacation.description}',
        destination='${vacation.destination}',
        startDate='${vacation.startDate}',
        endDate='${vacation.endDate}',
        price='${vacation.price}',
        followersCount='${vacation.followersCount}',
        picture='${vacation.picture}'
         WHERE vacationID=${vacation.vacationID}`;
    }
    else {
        sql = `UPDATE vacation SET
        vacationID='${vacation.vacationID}',
        description='${vacation.description}',
        destination='${vacation.destination}',
        startDate='${vacation.startDate}',
        endDate='${vacation.endDate}',
        price='${vacation.price}',
        followersCount='${vacation.followersCount}'
         WHERE vacationID=${vacation.vacationID}`;
    }
    notifyClients();
    const updataVacation =
        dal.executeQueryAsync(sql);
    return updataVacation;
};

function getReportAsync() {
    const report =
        dal.executeQueryAsync(
            "select DISTINCT v.destination as label, count(f.userID) as y from vacation as v left join followers as f on v.vacationID = f.vacationID GROUP BY v.vacationID HAVING y>0")
    return report;
}



module.exports = {
    getAllVacationAdminAsync,
    postVacation,
    deleteVacation,
    editVacation,
    getVacationByIdAsync,
    getReportAsync
}