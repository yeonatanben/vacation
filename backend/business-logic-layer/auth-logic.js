const jwt = require("jsonwebtoken");
const dal = require("../data-access-layer/dal");


async function loginAsync(credentials) {
    const user = await dal.executeQueryAsync(
        `
        select * from users
        where username=?
        and password=?
        `,
        [credentials.username,credentials.password]
        
    );
    if (!user || user.length < 1) return null;
    delete user[0].password;
    user[0].token = jwt.sign({ user: user[0] }, "yoni ben hur", { expiresIn: "10 minutes" });
    return user[0];
}

async function CheckUserName(name) {
    const user = await dal.executeQueryAsync(
        `
        select * from users
        where username=?
        `,
        [name]    
    );

    if (!user || user.length < 1) return true;
    return false;
}

async function registerAsync(reg) {
    try {
   
        const result = await dal.executeQueryAsync(
            "insert into users values (null,?,?,?,?,?)",[reg.firstName, reg.lastName, reg.username, reg.password, ""]
            );
        return result;
    }
    catch (error) {
        return error;
    }
}





module.exports = {
    loginAsync,
    registerAsync,
    CheckUserName
}