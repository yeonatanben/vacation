const Joi = require("joi");

class Registration {

    constructor(registration) {
        this.firstName = registration.firstName;
        this.lastName = registration.lastName;
        this.username = registration.username;
        this.password = registration.password;

    }

    static #validationSchema = Joi.object({
        firstName: Joi.string().required().min(2).max(10),
        lastName: Joi.string().required().min(2).max(10),
        username: Joi.string().required().min(4).max(15),
        password: Joi.string().required().min(2).max(15),

    });

    validate() {

        const result = Registration.#validationSchema.validate(this, { abortEarly: false });
        const errObj = {};
        if (result.error) {
            result.error.details.map(err => {
                errObj[err.path[0]] = err.message;
            });
            return errObj;
        }
        return this.null;
    }
}

module.exports = Registration;