const { boolean } = require("joi");
const Joi = require("joi");
class VacationModel {
    constructor(description,destination,
        startDate,endDate,price,picture){
            if (arguments.length === 6) {
                this.description =description;
                this.destination =destination;
                this.startDate= startDate;
                this.endDate = endDate;
                this.price=price;
                this.picture=picture;
            }
            else if (arguments.length === 1) {
                const vacation = arguments[0];
                this.description = vacation.description;
                this.destination= vacation.destination;
                this.startDate= vacation.startDate;
                this.endDate = vacation.endDate;
                this.price=vacation.price;
                this.picture=vacation.picture;
            }
            else
                throw "vacationModel structure error";
        }

        static#validationScheme=Joi.object({
            description:Joi.required(),
            destination:Joi.required(),
            startDate:Joi.required(),
            endDate:Joi.required(),
            price:Joi.required(),
            picture:Joi.required()
        });
        validate(){
            const result=VacationModel.#validationScheme.validate(this,{abortEarly: false});
            return result.error.details;
        }
}
module.exports=VacationModel;