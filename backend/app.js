const express = require("express");
const cors = require("cors");
// const chatLogic=require("./business-logic-layer/socket-logic");
const fileUpload = require("express-fileupload");
const socket = require("./business-logic-layer/socket-logic");
const vacationController = require("./controllers-layer/vacation-controller");
const adminController = require("./controllers-layer/admin-controller");
const authController = require("./controllers-layer/auth-controller");
// const vacationLogic = require("./business-logic-layer/vacation-logic");


const server = express();
server.use(cors());
server.use(express.json());
const listener = server.listen(4005, () => console.log("listening on 4005"));
socket.init(listener);


server.use("/vacation", vacationController);
server.use("/vacationAdmin", adminController);
server.use("/auth", authController);
server.use("/follow", vacationController);
server.use(express.static("images"));


server.use("*", (req, res) => {
    res.status(404).send(`Route not found ${req.originalUrl}`);
});

server.listen(4000, () => {
    console.log("Listening on 4000");
}).on("error", (err) => {
    if (err.code === "EADDRINUSE")
        console.log("Error: Address in use");
    else
        console.log("Error: Unknown error");
});





