const { request, response } = require("express");
const express = require("express");
const verifyLoggedIn = require("../middleware/verify-logged-in");
const vacationLogic = require("../business-logic-layer/vacation-logic");
const VacationModel = require("../model/VacationModel");

const router = express.Router();

router.get("/",verifyLoggedIn, async (request, response) => {
    try {
        const allVacation= await vacationLogic.getAllVacationAsync();
        response.send(allVacation);
    }
    catch (error) {
        response.status(500).send(error);
    }
});




router.get("/:id",verifyLoggedIn,async (request,response)=>{
try {
    const id=request.params.id;
    const vacation=await  vacationLogic.getVacationByIdAsync(id);
    // const vm = await new VacationModel(vacation[0]);
    response.send(vacation);
} catch (error) {
    response.status(500).send(error);
}
});


router.delete("/:vid/:uid",async(request,response)=>{
    const uid=request.params.uid;
    const vid=request.params.vid;
try{
const unFollow=await vacationLogic.unFolloAsync(uid,vid);
response.send(unFollow);
}catch(error){
    response.status(500).send(error);
}
});

router.post("/follow/:vid/:uid",async(request,response)=>{
    const uid=request.params.uid;
    const vid=request.params.vid;
try{
const follow=await vacationLogic.followAsync(uid,vid);
response.send(follow);
}catch(error){
    response.status(500).send(error);
}
});


module.exports = router;