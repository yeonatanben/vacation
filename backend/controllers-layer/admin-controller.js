const { request, response } = require("express");
const verifyLoggedIn = require("../middleware/verify-logged-in");
const verifyAdmin = require("../middleware/verify-admin");
const adminLogic = require("../business-logic-layer/adminLogic");
const VacationModel = require("../model/VacationModel");


const express = require("express");
const fs = require("fs");
const path = require("path");
const fileUpload = require("express-fileupload");



const router = express.Router();
router.use(fileUpload());


router.get("/",
    [verifyLoggedIn, verifyAdmin],
    async (request, response) => {
        try {
            const allVacation = await adminLogic.getAllVacationAdminAsync();
            response.send(allVacation);
        }
        catch (error) {
            response.status(500).send(error);
        }
    });


    router.get("/report", async(request, response) => {
        try {
            const report = await adminLogic.getReportAsync();
            response.send(report);
        } catch(error) {
            response.status(500).send(error);
        }
    });

router.get("/:id", async (request, response) => {
    const id = request.params.id
    try {
        const vacation = await adminLogic.getVacationByIdAsync(id);
        if (vacation)
            response.send(vacation);
        else
            response.status(404).send(`cen not find vacation ${request.params.id}`);
    }
    catch (error) {
        response.status(500).send(error);
    }
});





router.post("/", async (request, response) => {
    try {
        const newVacation = await new VacationModel(request.body);
        newVacation.picture = request.files.picture.name;
        const image = request.files.picture;
        const absolutePath = path.join(__dirname, "..", "images", newVacation.picture);
        await image.mv(absolutePath);
        console.log(absolutePath);
        adminLogic.postVacation(newVacation);
        response.status(201).send("created");
    } catch (error) {
        response.status(400).send(error);
    }
});


router.put("/:id", async (request, response) => {
    try {
        const editedVacation = await new VacationModel(request.body);
        editedVacation.vacationID = request.params.id;
        if (request.files && request.files.picture) {
            editedVacation.picture = request.files.picture.name;
            const image = request.files.picture;
            const absolutePath = path.join(__dirname, "..", "images", editedVacation.picture);
            await image.mv(absolutePath);
        }
        await adminLogic.editVacation(editedVacation);
        response.send("updated");
    } catch (error) {
        console.log(error);
        response.status(500).send(error);
    }
});

router.delete("/:id", async (request, response) => {
    const id = request.params.id
    try {
        const deleteVacation = await adminLogic.deleteVacation(id);
        response.send("delete ok");
    }
    catch (error) {
        console.log(error);
        response.status(500).send(error);
    }
});





router.post("/image", async (request, response) => {
    try {

        const vacation = new VacationModel(request.body);
        vacation.picture = request.files.picture.name;
        const image = request.files.picture;


        const absolutePath = path.join(__dirname, "..", "images", vacation.picture);

        await image.mv(absolutePath);   // mv = move
        console.log(absolutePath);
        adminLogic.postVacation//
        response.send("OK");
    }
    catch (error) {
        console.log(error);
        response.status(500).send(error);
    }
});


module.exports = router;
