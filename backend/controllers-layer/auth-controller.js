const { request, response } = require("express");
const Credentials = require("../model/Credentials");
const router = require("./vacation-controller");
const authLogic=require("../business-logic-layer/auth-logic");
const Registration=require("../model/registration");

router.post("/login", async (request, response) => {
    try {
        const credentials = new Credentials(request.body);
        const errors = credentials.validate();
        if (errors) return response.status(400).send(errors);
        const loggedInUser = await authLogic.loginAsync(credentials);
        if (!loggedInUser) return response.status(401).send("Incorect username or password");
        response.send(loggedInUser);
    } catch (error) {
        response.status(500).send(error.message);
    }
});

router.post("/register", async (request, response) => {
    try {
        const reg = new Registration(request.body);
        const errors = reg.validate();
        if (errors) {
            console.log(errors);
            response.status(400).json(errors);
        }
        else {
            const duplicateUser = await authLogic.CheckUserName(request.body.username);
            if  (!duplicateUser)   response.status(400).json('username already exist');
            const result = await authLogic.registerAsync(reg);
            console.log(result);
            response.json(result);
        }
    } catch (error) {
        response.status(500).send(error.message);
    }
});

module.exports = router;