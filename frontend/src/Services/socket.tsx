import { io, Socket } from "socket.io-client";
import AllVacation from "../model/Vacation";
import jwtAxios from "./jwtAxios";


class SocketService{
    public socket: Socket | undefined;

    public connect(updateClient: Function): void {
        
        this.socket = io("http://localhost:4005");
        this.socket.on("admin-change-something", () => {

            updateClient();
        
        });
        
    }

    public disconnect(): void {
        this.socket?.disconnect();
    }

    async getUpdataVacation(){
        const data = JSON.parse(localStorage.loginData);
        const id = data.userID;
        const response = await jwtAxios.get<AllVacation[]>(`http://localhost:4000/vacation/${id}`);
    }
    
    
}

const socketService = new SocketService();

export default socketService;