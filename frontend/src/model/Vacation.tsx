class AllVacation {

    vacationID: number;
    description: string;
    destination: string;
    startDate: Date;
    endDate: Date;
    price: number;
    userID: number;
    picture: FileList;
    countFollowers:number;
    
    constructor(vacationID: number,
        description: string,
        destination: string,
        startDate: Date,
        endDate: Date,
        price: number,
        userID: number,
        picture: FileList,
        countFollowers:number
    ) {
        this.vacationID = vacationID;
        this.description = description;
        this.destination = destination;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.userID = userID;
        this.picture = picture;
        this.countFollowers=countFollowers
    }
}
export default AllVacation


