class AddVacationVali {
    constructor(
        public description:string,
        public destination:string,
        public startDate:string,
        public endDate:string,
        public price:number,
        public picture:string,

    ){}
}
export default AddVacationVali