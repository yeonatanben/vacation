import axios from "axios";
import { Component, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import AllVacation from "../../model/Vacation";
import "./AddVacation.css";


interface AddVacationState {
    description: string,
    destination: string,
    startDate: string,
    endDate: string,
    price: number,
    picture: any;
    errors: any
}

class AddVacation extends Component<RouteComponentProps, AddVacationState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { description: "", destination: "", startDate: "", endDate: "", price: 0, picture: "", errors: {} }
    }

    public render(): JSX.Element {
        return (
            <div className="AddVacation">
                <h3>Add vacation</h3>

                
                <p>
                    <label htmlFor="">insert description: </label>
                    <input type="text" onChange={this.descriChanged} />
                    <span>{this.state.errors.description?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">insert destination: </label>
                    <input type="text" onChange={this.destinChanged} />
                    <span>{this.state.errors.destination?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">start date: </label>
                    <input type="date" onChange={this.startDateChanged} />
                    <span>{this.state.errors.startDate?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">end date: </label>
                    <input type="date" onChange={this.endDateChanged} />
                    <span>{this.state.errors.endDate?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">price: </label>
                    <input type="text" onChange={this.priceChanged} />
                    <span>{this.state.errors.price?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">Add img: </label>
                    <input type="file" onChange={this.picChanged} />
                    <span>{this.state.errors.picture?.toString()}</span>
                </p>
                <p>
                    <button onClick={this.save}>Add vacation</button>
                </p>
                <br />
                <p>
                <button onClick={this.back}>Back</button>
                </p>
            </div>
        );
    }
    descriChanged = (e: SyntheticEvent) => {
        this.setState({ description: String((e.target as HTMLInputElement).value) });
    }
    destinChanged = (e: SyntheticEvent) => {
        this.setState({ destination: String((e.target as HTMLInputElement).value) });
    }
    startDateChanged = (e: SyntheticEvent) => {
        this.setState({ startDate: String((e.target as HTMLInputElement).value) });
    }
    endDateChanged = (e: SyntheticEvent) => {
        this.setState({ endDate: String((e.target as HTMLInputElement).value) });
    }
    priceChanged = (e: SyntheticEvent) => {
        this.setState({ price: Number((e.target as HTMLInputElement).value) });
    }
    picChanged = (e: SyntheticEvent) => {
        this.setState({ picture: (e.target as HTMLInputElement).files });

    }


    save = async (e: SyntheticEvent) => {
        console.log(JSON.stringify(this.state));
        const start = new Date(this.state.startDate);
        const end = new Date(this.state.endDate);
        if (start > end){
            alert("Warning: start>end");
            this.props.history.push("/add")
        }
        const myFormData = new FormData();
        myFormData.append("description", this.state.description);
        myFormData.append("destination", this.state.destination);
        myFormData.append("startDate", this.state.startDate);
        myFormData.append("endDate", this.state.endDate);
        myFormData.append("price", this.state.price.toString());
        if (this.state.picture != "")
        myFormData.append("picture", this.state.picture[0]);
        
    

        try {
            const response = await axios.post<AllVacation>("http://localhost:4000/vacationAdmin", myFormData);
            this.setState({ description: "", destination: "", startDate: "", endDate: "", price: 0, errors: {} });
            alert("Vacation successfully added");
            this.props.history.push("/admin");
        } catch (error:any) {
            this.setState({ errors: error.response.data });
            console.log(JSON.stringify(error));
            alert("Vacation not added");
            this.props.history.push("/add");


        }


    }
    back=()=>{
        this.props.history.push("/admin");
    }

}

export default withRouter(AddVacation);
