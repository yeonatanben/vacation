import { Component } from "react";
import VacationView from "../VacationView/VacationView";
import "./hello.css";

class Hello extends Component {

    public render(): JSX.Element {
        return (
            <div className="hello">
                <h1>The best vacation site in Israel </h1><br />
                <h2>hello {this.name}</h2>


                <VacationView />
            </div>
        );
    }
    data = JSON.parse(localStorage.loginData);
    name = this.data.firstName;
}

export default Hello;
