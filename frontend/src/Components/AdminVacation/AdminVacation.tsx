import axios from "axios";
import Moment from 'moment';
import { Pencil, PlusLg, X, XLg } from 'react-bootstrap-icons';
import { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import AllVacation from "../../model/Vacation";
import jwtAxios from "../../Services/jwtAxios";
import "./AdminVacation.css";
import { Button } from "react-bootstrap";
import socketService from "../../Services/socket";



interface AdminVacationViewState {
    allVacation: AllVacation[];
    updateVacation: AllVacation[];
    vacationFromServer: AllVacation[];
    dataPoints: Array<{label:string, y:number}>;
}


class AdminVacationView extends Component<RouteComponentProps, AdminVacationViewState> {


    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { allVacation: [], updateVacation: [], vacationFromServer: [], dataPoints:[] }
    }
    public componentDidMount = async () => {

        this.getAdminVacation();
        

    }

    public render(): JSX.Element {
        return (
            <div className="VacationView">
                <h1>The Admin Page</h1>
                <p>
                    <label>to add vacation prase</label>
                    <Button onClick={this.add}><PlusLg /> </Button>
                </p>
                <p><button onClick={this.show}>To display graph</button></p>
                <div >
                    {this.state.allVacation.map(v =>
                        <div className="view" key={v.vacationID}>
                            <Button onClick={(e) => { this.edit(v.vacationID) }}><Pencil /></Button>
                            <Button onClick={(e) => { this.delete(v.vacationID) }}><XLg /></Button><br />
                            description: {v.description}<br />destiination: {v.destination} <br />
                            start: {Moment(v.startDate).format('DD/MM/yyyy')}<br />
                            end: {Moment(v.endDate).format('DD/MM/yyyy')}<br />
                            price: {v.price}$<br />
                            <span><img alt="No picture" width="190px" src={`http://localhost:4000/${v.picture}`}></img></span> <br />
                            count of followers: <strong>{v.countFollowers}</strong>

                        </div>)}
                </div>
            </div>
        );
    }

    getAdminVacation = async () => {
        try {
            const response = await jwtAxios.get<AllVacation[]>("http://localhost:4000/vacationAdmin");
            this.setState({ allVacation: response.data });
            console.log(response.data);
            
        } catch (error) {
            alert("Permission expired Re-sign in");
            this.props.history.push("/login");

        }

    }

  
    add = () => {
        this.props.history.push("/add");
    }

    show = () => {
        this.props.history.push("/report");
    }


    delete = async (id: number) => {
        try {
            const response = await axios.delete(`http://localhost:4000/vacationAdmin/${id}`);
            alert(response.data);
            this.getAdminVacation();
        } catch (error) {
            alert(error)
        }
    }


    edit = async (id: number) => {

        this.props.history.push(`/edit/${id}`);
    }
}

export default withRouter(AdminVacationView);