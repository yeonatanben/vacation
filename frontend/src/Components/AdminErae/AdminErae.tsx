import { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import AdminVacationView from "../AdminVacation/AdminVacation";
import "./AdminErae.css";

class AdminErae extends Component<RouteComponentProps> {

    public render(): JSX.Element {
        return (
            <div className="AdminErae">
				
                <AdminVacationView />
            </div>
        );
    }
}

export default withRouter(AdminErae);
