import { Component } from "react";
import "./Footer.css";

class Footer extends Component {

    public render(): JSX.Element {
        return (
            <div className="Footer">
				<span>&copy; All rights reserved to yoni ben hur</span>
            </div>
        );
    }
}

export default Footer;
