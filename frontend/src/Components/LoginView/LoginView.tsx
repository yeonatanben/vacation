import axios from "axios";
import { Component, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";

import "./LoginView.css";

interface LoginViewState {
    username: string;
    password: string;
    error:string
}

class LoginView extends Component<RouteComponentProps, LoginViewState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { username: "", password: "",error:"" };
    }

    public render(): JSX.Element {
        return (
            <div className="LoginView">
                <h2>Login</h2>
                <p>
                    <input placeholder="username" onChange={this.usernameChanged} />
                </p>
                <p>
                    <input placeholder="password" onChange={this.passwordChanged} />
                </p>
                <p>
                    <button onClick={this.login}>Login</button>
                </p>
                <p></p>


            </div>
        );
    }

    private usernameChanged = (e: SyntheticEvent) => {
        const username = (e.target as HTMLInputElement).value;
        this.setState({ username });
    }

    private passwordChanged = (e: SyntheticEvent) => {
        const password = (e.target as HTMLInputElement).value;
        this.setState({ password });
    }

    login = async (e: SyntheticEvent) => {
        try {
            const response = await axios.post("http://localhost:4000/auth/login", { username: this.state.username, password: this.state.password });
            localStorage["loginData"] = JSON.stringify(response.data);
            
            
            if(response.data.role){
                this.props.history.push("/admin"); 
            }
            else{
            this.props.history.push("/vacation");
            }
        } catch (error) {
            this.props.history.push("/register");
            alert("You're not registered or one of the details isn't true")
        }
    
        this.setState({ username: "", password: "", });
        

    }
}

export default withRouter(LoginView);

