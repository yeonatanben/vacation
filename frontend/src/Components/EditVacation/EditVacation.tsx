import axios from "axios";
import { Component, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import AllVacation from "../../model/Vacation";
import "./EditVacation.css";


interface EditVacationState {
    description: string,
    destination: string,
    startDate: string,
    endDate: string,
    price: number,
    picture: any,
    errors: any
}

class EditVacation extends Component<RouteComponentProps<any>, EditVacationState> {
    private vacationID = -1;
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { description: "", destination: "", startDate: "", endDate: "", price: 0, picture: "", errors: {} }
    }
    public componentDidMount = async () => {
        try {
            this.vacationID = this.props.match.params.id;

            const response = await axios.get(`http://localhost:4000/vacationAdmin/${this.vacationID}`);
            (this.refs.description as HTMLInputElement).value = response.data[0].description;
            (this.refs.destination as HTMLInputElement).value = response.data[0].destination;
            (this.refs.startDate as HTMLInputElement).value = response.data[0].startDate.substring(0, response.data[0].startDate.indexOf('T'));
            (this.refs.endDate as HTMLInputElement).value = response.data[0].endDate.substring(0, response.data[0].endDate.indexOf('T'));
            (this.refs.price as HTMLInputElement).value = response.data[0].price;
            this.setState({ description: response.data[0].description });
            this.setState({ destination: response.data[0].destination });
            this.setState({ startDate: response.data[0].startDate.substring(0, response.data[0].startDate.indexOf('T')) });
            this.setState({ endDate: response.data[0].endDate.substring(0, response.data[0].endDate.indexOf('T')) });
            this.setState({ price: response.data[0].price });

        } catch (error) {
            alert(error);
        }
    }


    public render(): JSX.Element {
        return (
            <div className="AddVacation">
                <h3>Edit vacation</h3>

                
                <p>
                    <label htmlFor="">change description: </label>
                    <input ref="description" type="text" onChange={this.descriChanged} />
                    <span>{this.state.errors.description?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">change destination: </label>
                    <input ref="destination" type="text" onChange={this.destinChanged} />
                    <span>{this.state.errors.destination?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">change start date: </label>
                    <input ref="startDate" type="date" onChange={this.startDateChanged} />
                    <span>{this.state.errors.startDate?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">change end date: </label>
                    <input ref="endDate" type="date" onChange={this.endDateChanged} />
                    <span>{this.state.errors.endDate?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">change price: </label>
                    <input ref="price" type="text" onChange={this.priceChanged} />
                    <span>{this.state.errors.price?.toString()}</span>
                </p>
                <p>
                    <label htmlFor="">change img: </label>
                    <input type="file" onChange={this.picChanged} />
                    <span>{this.state.errors.picture?.toString()}</span>
                </p>
                <p>
                    <button onClick={this.save}>edit vacation</button>
                </p>
                <br />
                <p>
                <button onClick={this.back}>Back</button>
                </p>
            </div>
        );
    }
    descriChanged = (e: SyntheticEvent) => {
        this.setState({ description: String((e.target as HTMLInputElement).value) });

    }
    destinChanged = (e: SyntheticEvent) => {
        this.setState({ destination: String((e.target as HTMLInputElement).value) });
    }
    startDateChanged = (e: SyntheticEvent) => {
        this.setState({ startDate: String((e.target as HTMLInputElement).value) });
    }
    endDateChanged = (e: SyntheticEvent) => {
        this.setState({ endDate: String((e.target as HTMLInputElement).value) });
    }
    priceChanged = (e: SyntheticEvent) => {
        this.setState({ price: Number((e.target as HTMLInputElement).value) });
    }
    picChanged = (e: SyntheticEvent) => {
        this.setState({ picture: (e.target as HTMLInputElement).files });

    }


    save = async (e: SyntheticEvent) => {
        const start = new Date(this.state.startDate);
        const end = new Date(this.state.endDate);
        if (start > end)
            console.log("Error: start>end");
        const myFormData = new FormData();
        console.log(this.state);

        myFormData.append("vacationID", this.vacationID.toString());
        myFormData.append("description", this.state.description);
        myFormData.append("destination", this.state.destination);
        myFormData.append("startDate", this.state.startDate);
        myFormData.append("endDate", this.state.endDate);
        myFormData.append("price", this.state.price.toString());
        if (this.state.picture != "")
            myFormData.append("picture", this.state.picture[0]);
        


        try {
            const response = await axios.put<AllVacation>(`http://localhost:4000/vacationAdmin/${this.vacationID}`, myFormData);
            this.setState({ description: "", destination: "", startDate: "", endDate: "", price: 0, errors: {} });
            alert("put ok")
            this.props.history.push("/admin");
        } catch (error) {
            this.setState({ errors: error });
            console.log(JSON.stringify(error));
            alert("put not ok");
            this.props.history.push("/edit/:id" );

        }


    }
    back=()=>{
        this.props.history.push("/admin");
    }

}


export default withRouter(EditVacation);
