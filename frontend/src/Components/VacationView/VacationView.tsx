import Moment from 'moment';
import { Component } from "react";
import AllVacation from "../../model/Vacation";
import jwtAxios from "../../Services/jwtAxios";
import socketService from "../../Services/socket";
import { RouteComponentProps, withRouter } from 'react-router-dom'
import "./VacationView.css";
import { Button } from "react-bootstrap";
import { Heart } from "react-bootstrap-icons";


interface VacationViewState {
    allVacation: AllVacation[],


}

class VacationView extends Component<RouteComponentProps, VacationViewState> {

    

    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { allVacation: [] }
    }
    public componentDidMount = async () => {

        this.getAllVacation();
        socketService.connect(() => { this.getAllVacation() });

    }



    public render(): JSX.Element {
        return (
            <div className="VacationView">


                <div >
                    <Button  onClick={this.logout}>log-out</Button><br />
                    {this.state.allVacation.map(v =>
                        <div className="view" key={v.vacationID}>
                            <div>destination:</div><span className="des">{v.destination}</span> <div>description:</div> {v.description}<br />
                            start: {Moment(v.startDate).format('DD/MM/yyyy')}<br />
                            end: {Moment(v.endDate).format('DD/MM/yyyy')}<br />
                            price: {v.price} $<br /><br />
                            <span><img alt="No picture" width="190px" src={`http://localhost:4000/${v.picture}`}></img></span> <br />
                            <Button className={v.userID ? 'follow' : 'not-follow'} onClick={(e) => { this.follow(v.vacationID, v.userID) }}><Heart /> follow</Button>

                        </div>)}
                </div>
            </div>
        );
    }


    follow = async (vid: number, uid: number) => {

        if (uid) {
            try {
                const response = await jwtAxios.delete(`http://localhost:4000/vacation/${vid}/${uid}`);
                this.getAllVacation();
            } catch (error) {
                console.log(error);

            }
        }
        else {
            try {

                const data = JSON.parse(localStorage.loginData);
                const uid = data.userID;

                const response = await jwtAxios.post(`http://localhost:4000/vacation/follow/${vid}/${uid}`);
                this.getAllVacation();
            } catch (error) {
                console.log(error);
                ;
            }
        }
    }
    getAllVacation = async () => {
        const data = JSON.parse(localStorage.loginData);
        const id = data.userID;
        try {
            const response = await jwtAxios.get<AllVacation[]>(`http://localhost:4000/vacation/${id}`);
            this.props.history.push("/hello");
            this.setState({ allVacation: response.data });
        } catch (error) {
            alert("Permission expired Re-sign in");
            this.props.history.push("/login");
        }
    }
    logout=() => {
        localStorage.clear();
        this.props.history.push("/")
    }

}

export default withRouter(VacationView);
