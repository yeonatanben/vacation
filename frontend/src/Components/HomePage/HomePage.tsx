import { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import LoginView from "../LoginView/LoginView";
import "./HomePage.css";

class HomePage extends Component<RouteComponentProps> {


    constructor(props: RouteComponentProps) {
        super(props);

    }

    public render(): JSX.Element {
        return (
            <div className="HomePage">
<h1>wellcome to vacation site</h1>

<LoginView />

<p>
    <label>  Are you not registered yet? <br />To register click</label><br />
    <button onClick={this.register}>register</button>
</p>
            </div>
        );
    }

    register =()=>{
        this.props.history.push("/register")
    }
}

export default withRouter (HomePage);
