import { Component } from "react";
import LoginView from "../LoginView/LoginView";
import RegisterView from "../RegisterView/RegisterView";
import VacationView from "../VacationView/VacationView";
import { Redirect, Route, Switch } from 'react-router';
import "./Main.css";
import AdminErae from "../AdminErae/AdminErae";
import EditVacation from "../EditVacation/EditVacation";
import HomePage from "../HomePage/HomePage";
import AddVacation from "../AddVacation/AddVacation";
import Hello from "../hello/hello";
import Report from "../Report/Report";
import NotFound from "../NotFound/NotFound";

class Main extends Component {

    public render(): JSX.Element {
        return (
            <div className="Main">
                <h2></h2>

                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/vacation" exact component={VacationView} />
                    <Route path="/login" exact component={LoginView} />
                    <Route path="/register" exact component={RegisterView} />
                    <Route path="/admin" exact component={AdminErae} />
                    <Route path="/add" exact component={AddVacation} />
                    <Route path="/edit/:id" exact component={EditVacation} />
                    <Route path="/hello" exact component={Hello} />
                    <Route path="/report" exact component={Report} />
                    <Redirect from="/" exact to="/home" />
                    <Route path="*" component={NotFound} />
                </Switch>




            </div>
        );
    }

}

export default Main;
