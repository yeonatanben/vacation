import { Component } from "react";
import { BrowserRouter, NavLink } from "react-router-dom";
import "./Menu.css";

class Menu extends Component {

    public render(): JSX.Element {
        return (
            <div className="Menu">
			  <nav>
                  <br /><br /><br /><br /><br /><br /><br />
                  
                  
                
                  <NavLink to="/">Home</NavLink>&nbsp;&nbsp;<br /><br />
                <NavLink to="/login">Log-in</NavLink>&nbsp;&nbsp;<br /><br />
                <NavLink to="/register">Register</NavLink>&nbsp;&nbsp;<br /><br />
                <NavLink to="/vacation">Vacation</NavLink>&nbsp;&nbsp;<br /><br />
                
            </nav>
            </div>
        );
    }
    
}

export default Menu;
