import { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import Main from "../Main/Main";
import Menu from "../Menu/Menu";
import "./Layout.css";

class Layout extends Component {

    public render(): JSX.Element {
        return (
            <BrowserRouter>
            <div className="Layout">
                 
                <header>
                    <Header />
                </header>
                <aside>
                    <Menu />
                </aside>
                <main>
                    <Main />
                </main>
                <footer>
                    <Footer />
                </footer>
				
            </div>
            </BrowserRouter>
        );
    }
}

export default Layout;
