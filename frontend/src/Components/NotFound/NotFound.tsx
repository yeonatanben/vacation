import { Component } from "react";
import "./NotFound.css";

class NotFound extends Component {

    public render(): JSX.Element {
        return (
            <div className="NotFound">
				<h1>URL Not Found (404)</h1>
            </div>
        );
    }
}

export default NotFound;
