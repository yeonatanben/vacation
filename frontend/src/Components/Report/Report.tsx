import { Component } from "react";
import "./Report.css";
import CanvasJSReact from "../../canvasjs.react";
import { RouteComponentProps, withRouter } from "react-router";
import jwtAxios from "../../Services/jwtAxios";


var CanvasJSChart = CanvasJSReact.CanvasJSChart;

interface ReportsState{
    dataPoints: Array<{label:string, y:number}>;
    errorMessage: string|null;
    errorOpen: boolean;
}


class Reports extends Component <RouteComponentProps,ReportsState>{

    constructor(props:RouteComponentProps,){
        super(props);
        this.state= {dataPoints:[], errorMessage: null, errorOpen: false};
    }

    componentDidMount = async () => { 
        try {
            const response = await jwtAxios.get<Array<{label:string, y:number}>>(`http://localhost:4000/vacationAdmin/report`);
            this.setState({dataPoints: response.data});
            
        } catch (error) {
            this.setState({errorMessage:"error" , errorOpen: true});//notify.error(error)
        }
    }

    public render(): JSX.Element {        
         const options = {
            data: [{				
                type: "column",
                dataPoints:this.state.dataPoints
            }]
         }
        
        
         
        return (            
            <div className="reports">
                <CanvasJSChart options={options} className="report"
                
                />
                <br />
                <p>
                <button onClick={this.back}>Back</button>
                </p>
           
            </div>
        );
    }
    back=()=>{
        this.props.history.push("/admin");
    }
}

export default withRouter (Reports);