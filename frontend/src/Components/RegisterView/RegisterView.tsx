import axios from "axios";
import { timeStamp } from "console";
import { Component, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import "./RegisterView.css";

interface RegisterViewState {
    firstName:string;
    lastName:string;
    username:string;
    password:string;
    errors:any;
}

class RegisterView extends Component<RouteComponentProps, RegisterViewState> {

    constructor(props:RouteComponentProps) {
        super(props);
        this.state={firstName:"",lastName:"", username:"", password:"", errors:{}};
    }

    public render(): JSX.Element {
        return (
            <div className="RegisterView">
                <h2>register page</h2>
                <p>
                    <input placeholder="firstName" onChange={this.firstNameChanged}  value={this.state.firstName}/>
                    <span>{this.state.errors.firstName?.toString()}</span>
                </p>
                <p>
                    <input placeholder="lastName" onChange={this.lastNameChanged}  value={this.state.lastName}/>
                    <span>{this.state.errors.lastName?.toString()}</span>
                </p>
				<p>
                    <input placeholder="Username" onChange={this.usernameChanged} value={this.state.username}/>
                    {this.state.errors.username && <span>{this.state.errors.username}</span>}
                </p>
                <p>
                    <input placeholder="Password" onChange={this.passwordChanged}  value={this.state.password}/>
                    {this.state.errors.password && <span>{this.state.errors.password}</span>}
                </p>
                
                <p>
                    <button onClick={this.register}>Register</button>
                </p>
            </div>
        );
    }
   private firstNameChanged = (e: SyntheticEvent) => {
       const firstName = (e.target as HTMLInputElement).value;
       this.setState({ firstName });
    };

    private lastNameChanged = (e: SyntheticEvent) => {
        const lastName = (e.target as HTMLInputElement).value;
        this.setState({ lastName });
    };

    private usernameChanged = (e: SyntheticEvent) => {
        const username = (e.target as HTMLInputElement).value;
        this.setState({ username });
    };

    private passwordChanged = (e: SyntheticEvent) => {
        const password = (e.target as HTMLInputElement).value;
        this.setState({ password });
    };

 

    private register = async (e: SyntheticEvent) => {
        try {
            const response = await axios.post("http://localhost:4000/auth/register",
             {firstName:this.state.firstName,lastName:this.state.lastName, username: this.state.username, password: this.state.password });    
            this.setState({errors:{}});
            alert("You signed up successfully You are transferred to the Login page")
            this.props.history.push("/login");
            //clear input...
        this.setState({firstName:"",lastName:"",username:"",password:""})
            // console.log(response.data);
            
        } catch (error:any) {
            this.setState({errors:error.response.data});
            console.log(JSON.stringify(error.response.data));
        }
        
    }
}

export default withRouter (RegisterView);
